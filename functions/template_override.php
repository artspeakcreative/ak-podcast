<?php

add_filter('template_include', 'ak_podcast_override_templates');
function ak_podcast_override_templates($template)
{
 global $AK_PODCAST_ROOT;

 if (is_post_type_archive('podcast')) {
  $theme_files = array('archive-podcast.php', 'ak-podcast/archive-podcast.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_PODCAST_ROOT . '/templates/archive-podcast.php';
  }
 } elseif (is_singular('podcast')) {
  $theme_files = array('single-podcast.php', 'ak-podcast/single-podcast.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_PODCAST_ROOT . '/templates/single-podcast.php';
  }
 } elseif (is_tax('podcast_name')) {
  $theme_files = array('taxonomy-podcast_name.php', 'ak-podcasts/taxonomy-podcast_name.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_PODCAST_ROOT . '/templates/taxonomy-podcast_name.php';
  }
 } elseif (is_tax('podcast_category')) {
  $theme_files = array('taxonomy-podcast_category.php', 'ak-podcasts/taxonomy-podcast_category.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_PODCAST_ROOT . '/templates/taxonomy-podcast_category.php';
  }
 } elseif (is_tax('podcast_host')) {
  $theme_files = array('taxonomy-podcast_host.php', 'ak-podcasts/taxonomy-podcast_host.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_PODCAST_ROOT . '/templates/taxonomy-podcast_host.php';
  }
 } elseif (is_tax('podcast_guest')) {
  $theme_files = array('taxonomy-podcast_guest.php', 'ak-podcasts/taxonomy-podcast_guest.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_PODCAST_ROOT . '/templates/taxonomy-podcast_guest.php';
  }
 } elseif (is_tax('podcast_editor')) {
  $theme_files = array('taxonomy-podcast_editor.php', 'ak-podcasts/taxonomy-podcast_editor.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_PODCAST_ROOT . '/templates/taxonomy-podcast_editor.php';
  }
 } elseif (is_tax('podcast_musician')) {
  $theme_files = array('taxonomy-podcast_musician.php', 'ak-podcasts/taxonomy-podcast_musician.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_PODCAST_ROOT . '/templates/taxonomy-podcast_musician.php';
  }
 }
 return $template;
}
