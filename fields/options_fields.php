<?php
/* ---------------------
 * ACF Settings Function
 * --------------------- */
function ak_podcast_add_options()
{
 if (function_exists('acf_add_local_field_group')) {
  $group_key = 'options_ak_podcast';
  $group = array(
   'key' => $group_key,
   'title' => "Podcast Settings",
   'fields' => array(),
   'location' => array(
    array(
     array(
      'param' => 'options_page',
      'operator' => '==',
      'value' => 'acf-options',
     ),
    ),
   ),
   'menu_order' => 0,
   'position' => 'normal',
   'style' => 'default',
   'label_placement' => 'top',
   'instruction_placement' => 'label',
   'hide_on_screen' => '',
  );

  acf_add_local_field_group($group);

  // acf_add_local_field(array(
  //  'key' => $group_key . "_ical_url",
  //  'label' => 'iCal URL',
  //  'name' => 'ical_url',
  //  'type' => 'text',
  //  'parent' => $group_key,
  // ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_soundtrack_zone",
  //    'label' => 'Soundtrack Zone',
  //    'name' => 'soundtrack_zone',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_zone",
  //    'label' => 'Carousel Zone',
  //    'name' => 'carousel_zone',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_url",
  //    'label' => 'Carousel API URL',
  //    'name' => 'carousel_api_url',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_user",
  //    'label' => 'Carousel User',
  //    'name' => 'carousel_user',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_pass",
  //    'label' => 'Carousel Password',
  //    'name' => 'carousel_pass',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_group",
  //    'label' => 'Carousel Group ID',
  //    'name' => 'carousel_group',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_tag_id",
  //    'label' => 'Carousel tag ID',
  //    'name' => 'carousel_tag',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));
 }
}
add_filter('init', 'ak_podcast_add_options');
