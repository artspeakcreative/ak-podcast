<?php

function ak_podcast_shortcodes()
{
 global $AK_PODCAST_ROOT;
 $plugin_components = $AK_PODCAST_ROOT . '/templates/views/components/';
 Component::load_components($plugin_components);
}
add_action('ak_run_actions', 'ak_podcast_shortcodes');
