<?php
/* ---------------------
 * ACF Settings Function
 * --------------------- */
function ak_podcast_add_podcast_name_fields()
{
 if (function_exists('acf_add_local_field_group')) {
  $group_key = 'ak_podcast_name_fields';
  $group = array(
   'key' => $group_key,
   'title' => "Additional Podcast Settings",
   'fields' => array(),
   'location' => array(
    array(
     array(
      'param' => 'taxonomy',
      'operator' => '==',
      'value' => 'podcast_name',
     ),
    ),
   ),
   'menu_order' => 0,
   'position' => 'normal',
   'style' => 'default',
   'label_placement' => 'top',
   'instruction_placement' => 'label',
   'hide_on_screen' => '',
  );

  acf_add_local_field_group($group);

  acf_add_local_field(array(
   'key' => $group_key . "_ways_to_listen_repeater",
   'label' => 'Ways To Listen',
   'name' => 'ways_to_listen',
   'type' => 'repeater',
   'sub_fields' => array(
    array(
     'key' => $group_key . "_wtl_name",
     'label' => 'Name',
     'name' => 'name',
     'type' => 'text',
     'parent' => $group_key,
    ),
    array(
     'key' => $group_key . "_wtl_url",
     'label' => 'URL',
     'name' => 'url',
     'type' => 'url',
     'parent' => $group_key,
    ),
   ),
   'parent' => $group_key,
  ));
 }
}
add_filter('init', 'ak_podcast_add_podcast_name_fields');
