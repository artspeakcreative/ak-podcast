<?php
namespace PostType;

/*
Podcast Post Type
 */

class Podcast extends BasePost
{
 const POST_TYPE = "Podcast";

 /**- - - - - - - - - - - - - -
  * CONSTRUCTOR
  * - - - - - - - - - - - - - - */
 public function __construct($post)
 {
  parent::__construct($post);

  $this->info = array(
   'title' => $this->title,
   'date' => $this->podcast_date,
  );

  $this->podcast_date = $this->date();
  $this->pod_date = new \DateTimeImmutable($this->podcast_date, wp_timezone());
  $this->podcast_name = $this->get_podcast_name();
  $this->season = $this->get_season();
  $this->host = $this->get_host();
  $this->hosts = $this->get_hosts();
  $this->guest = $this->get_guest();
  $this->guests = $this->get_guests();
  $this->edited_by = $this->get_edited_by();
  $this->music_by = $this->get_music_by();
  $this->mp3 = wp_get_attachment_url($this->audio);
  if (!$this->mp3) {
   $this->mp3 = $this->audio_url;
  }
 }

 /**- - - - - - - - - - - - - -
  * STATIC FUNCTIONS
  * - - - - - - - - - - - - - - */

 /**
  * Initial setup of all static functions
  *
  * @return void
  */
 public static function setup()
 {
  Podcast::register();
  Podcast::fields();
  Podcast::hooks();
 }

 /**
  * Register the class in WP
  *
  * @return void
  */
 public static function register()
 {
  register_extended_post_type(self::POST_TYPE, array(
   'show_in_feed' => true,
   'menu_icon' => 'dashicons-microphone',
   'enter_title_here' => 'Podcast Title',
   'admin_cols' => array(
    'featured_image' => array(
     'title' => 'Image',
     'featured_image' => 'tiny',
    ),
    'title',
    'supports' => [
     'custom-fields',
    ],
    'podcast_date' => array(
     'title' => 'Podcast Date',
     'meta_key' => 'podcast_date',
     'default' => 'DESC',
    ),
    // A taxonomy terms column:
    'podcast_name' => array(
     'taxonomy' => 'podcast_name',
    ),
    'podcast_host' => array(
     'taxonomy' => 'podcast_host',
    ),
   ),
   'admin_filters' => array(
    'podcast_name' => array(
     'title' => 'Podcast Name',
     'taxonomy' => 'podcast_name',
    ),
    'podcast_category' => array(
     'title' => 'Category',
     'taxonomy' => 'podcast_category',
    ),
    'podcast_host' => array(
     'title' => 'Host',
     'taxonomy' => 'podcast_host',
    ),

   ),
  ), array(
   # Override the base names used for labels:
   'singular' => 'Podcast',
   'plural' => 'Podcasts',
   'slug' => 'podcasts',
  ));

  register_extended_taxonomy(
   'podcast_name',
   strtolower(self::POST_TYPE),
   array(),
   array(
    # Override the base names used for labels:
    'singular' => 'Podcast Name/Season',
    'plural' => 'Podcast Names/Seasons',
    'slug' => 'podcast_names',
   ));

  register_extended_taxonomy(
   'podcast_category',
   strtolower(self::POST_TYPE),
   array(),
   array(
    # Override the base names used for labels:
    'singular' => 'Podcast Category',
    'plural' => 'Podcast Categories',
    'slug' => 'podcast_categories',
   ));

  register_extended_taxonomy(
   'podcast_host',
   strtolower(self::POST_TYPE),
   array(),
   array(
    # Override the base names used for labels:
    'singular' => 'Host',
    'plural' => 'Hosts',
    'slug' => 'podcast_hosts',
   ));

  register_extended_taxonomy(
   'podcast_guest',
   strtolower(self::POST_TYPE),
   array(),
   array(
    # Override the base names used for labels:
    'singular' => 'Guest',
    'plural' => 'Guests',
    'slug' => 'podcast_guests',
   ));

  register_extended_taxonomy(
   'podcast_editor',
   strtolower(self::POST_TYPE),
   array(),
   array(
    # Override the base names used for labels:
    'singular' => 'Editor',
    'plural' => 'Editors',
    'slug' => 'podcast_editors',
   ));

  register_extended_taxonomy(
   'podcast_musician',
   strtolower(self::POST_TYPE),
   array(),
   array(
    # Override the base names used for labels:
    'singular' => 'Musician',
    'plural' => 'Musicians',
    'slug' => 'podcast_musicians',
   ));
 }

 /**
  * Add all the ACF fields to the post type
  *
  * @return void
  */
 public static function fields()
 {
  $group_key = 'group_' . str_replace(' ', '_', self::POST_TYPE) . '_post_type';
  $group = array(
   'key' => $group_key,
   'title' => self::POST_TYPE . " Settings",
   'fields' => array(),
   'location' => array(
    array(
     array(
      'param' => 'post_type',
      'operator' => '==',
      'value' => strtolower(self::POST_TYPE),
     ),
    ),
   ),

   'menu_order' => 0,
   'position' => 'normal',
   'style' => 'default',
   'label_placement' => 'top',
   'instruction_placement' => 'label',
   'hide_on_screen' => '',
  );

  acf_add_local_field_group($group);

  acf_add_local_field(array(
   'key' => $group_key . "_podcast_date",
   'label' => 'Date',
   'name' => 'podcast_date',
   'type' => 'date_picker',
   'display_format' => 'Y-m-d',
   'return_format' => 'Y-m-d',
   'first_day' => 0,
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_episode_number",
   'label' => 'Episode Number',
   'name' => 'episode_number',
   'type' => 'number',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_audio",
   'label' => 'Audio',
   'name' => 'audio',
   'type' => 'file',
   'return_format' => 'id',
   'mime_types' => 'mp3',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_audio_url",
   'label' => 'Audio Url',
   'name' => 'audio_url',
   'type' => 'url',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_header_image",
   'label' => 'Header Image',
   'name' => 'header_image',
   'type' => 'image',
   'return_format' => 'id',
   'parent' => $group_key,
  ));

  // acf_add_local_field(array(
  //  'key' => $group_key . "_notes",
  //  'label' => 'Episode Notes',
  //  'name' => 'notes',
  //  'type' => 'wysiwyg',
  //  'parent' => $group_key,
  // ));

  acf_add_local_field(array(
   'key' => $group_key . "_excerpt",
   'label' => 'Excerpt/Tagline',
   'name' => 'excerpt',
   'type' => 'wysiwyg',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_episode_links",
   'label' => 'Episode Links',
   'name' => 'episode_links',
   'type' => 'repeater',
   'sub_fields' => array(
    array(
     'key' => $group_key . "_episode_links_name",
     'label' => 'Name',
     'name' => 'episode_link_name',
     'type' => 'text',
    ),
    array(
     'key' => $group_key . "_episode_link_url",
     'label' => 'URL',
     'name' => 'episode_link_url',
     'type' => 'url',
    ),
   ),
   'parent' => $group_key,
  ));

  // acf_add_local_field(array(
  //  'key' => $group_key . "_quotes",
  //  'label' => 'Quotes',
  //  'name' => 'episode_quotes',
  //  'type' => 'repeater',
  //  'sub_fields' => array(
  //   array(
  //    'key' => $group_key . "_quote_name",
  //    'label' => 'Name',
  //    'name' => 'quote_name',
  //    'type' => 'text',
  //   ),
  //   array(
  //    'key' => $group_key . "_quote_text",
  //    'label' => 'Quote',
  //    'name' => 'quote_text',
  //    'type' => 'wysiwyg',
  //   ),
  //  ),
  //  'parent' => $group_key,
  // ));

  // acf_add_local_field(array(
  //  'key' => $group_key . "_shareables",
  //  'label' => 'Shareables',
  //  'name' => 'episode_shareables',
  //  'type' => 'repeater',
  //  'sub_fields' => array(
  //   array(
  //    'key' => $group_key . "_shareable_name",
  //    'label' => 'Name',
  //    'name' => 'shareable_name',
  //    'type' => 'text',
  //   ),
  //   array(
  //    'key' => $group_key . "_shareable_image",
  //    'label' => 'Image',
  //    'name' => 'shareable_image',
  //    'type' => 'image',
  //   ),
  //  ),
  //  'parent' => $group_key,
  // ));

  acf_add_local_field(array(
   'key' => $group_key . "_transcript",
   'label' => 'Transcript',
   'name' => 'transcript',
   'type' => 'wysiwyg',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_related_episodes",
   'label' => 'Related Episodes',
   'name' => 'related_episodes',
   'type' => 'post_object',
   'multiple' => 1,
   'return_format' => 'id',
   'post_type' => array(
    0 => 'podcast',
   ),
   'parent' => $group_key,
  ));

  // acf_add_local_field(array(
  //  'key' => $group_key . "_related_content",
  //  'label' => 'Related Content',
  //  'name' => 'related_content',
  //  'type' => 'post_object',
  //  'multiple' => 1,
  //  'return_format' => 'id',
  //  'parent' => $group_key,
  // ));
 }

 public static function query_includes_podcasts($query)
 {
  if (is_array($query->query_vars['post_type'])) {
   if (in_array('podcast', $query->query_vars['post_type'])) {
    return true;
   }
  } elseif ($query->query_vars['post_type'] == 'podcast') {
   return true;
  } elseif ($query->query_vars['podcast_name'] || $query->query_vars['podcast_category'] || $query->query_vars['podcast_host'] || $query->query_vars['podcast_guest'] || $query->query_vars['podcast_editor'] || $query->query_vars['podcast_musician']) {
   return true;
  }
  return false;
 }

 public static function ak_sort_podcasts_by_date($query)
 {
  if (Podcast::query_includes_podcasts($query) && is_admin() == false) {
   $query->set('orderby', 'meta_value');
   $query->set('meta_key', 'podcast_date');
   $query->set('order', 'DESC');
  }
 }

 public static function hooks()
 {
  add_action('pre_get_posts', 'PostType\Podcast::ak_sort_podcasts_by_date', 1);
 }

 /**- - - - - - - - - - - - - -
  * HELPER FUNCTIONS
  * - - - - - - - - - - - - - - */
 public function data()
 {
  return array(
   //'title' => $this->title
   'title' => $this->title,
   'date' => $this->date(),
  );
 }

 public function date($format = 'M d')
 {
  $timestamp = strtotime($this->podcast_date);
  return date($format, $timestamp);
 }

 public function get_podcast_name()
 {
  if ($this->podcast_name) {return $this->podcast_name;}
  $names = wp_get_post_terms($this->ID, 'podcast_name');
  foreach ($names as $name) {
   if ($name->parent == 0) {
    $this->podcast_name = $name;
    return $name;
   }
  }
 }

 public function get_season()
 {
  if ($this->season) {return $this->season;}
  $names = wp_get_post_terms($this->ID, 'podcast_name');
  foreach ($names as $name) {
   if ($name->parent > 0) {
    $this->season = $name;
    return $name;
   }
  }
 }

 public function get_host()
 {
  if ($this->host) {return $this->host;}
  $host = wp_get_post_terms($this->ID, 'podcast_host');
  $host = array_shift($host);
  $this->host = $host;
  return $host;
 }

 public function get_hosts()
 {
  if ($this->hosts) {return $this->hosts;}
  $hosts = wp_get_post_terms($this->ID, 'podcast_host');
  $this->hosts = $hosts;
  return $hosts;
 }

 public function get_guest()
 {
  if ($this->guest) {return $this->guest;}
  $guest = wp_get_post_terms($this->ID, 'podcast_guest');
  $guest = array_shift($guest);
  $this->guest = $guest;
  return $guest;
 }

 public function get_guests()
 {
  if ($this->guests) {return $this->guests;}
  $guests = wp_get_post_terms($this->ID, 'podcast_guest');
  $this->guests = $guests;
  return $guests;
 }

 public function get_edited_by()
 {
  if ($this->edited_by) {return $this->edited_by;}
  $editors = wp_get_post_terms($this->ID, 'podcast_editor');
  $this->edited_by = $editors;
  return $editors;
 }

 public function get_music_by()
 {
  if ($this->music_by) {return $this->music_by;}
  $music = wp_get_post_terms($this->ID, 'podcast_musician');
  $this->music_by = $music;
  return $music;
 }
}

Podcast::setup();
