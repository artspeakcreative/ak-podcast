<?php

function ak_podcast_add_to_context($context)
{
 $context['latest_podcast'] = array_shift(Timber::get_posts(['post_type' => 'podcast', 'number_posts' => 1]));

 return $context;
}
add_filter('akmt_add_to_context', 'ak_podcast_add_to_context');
