<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array('archive.twig', 'index.twig');

$context = Timber::get_context();

$context['title'] = 'Podcasts';
$postArgs = array(
 'post_type' => 'podcast',
 'numberposts' => -1,
 'order' => 'DESC',
 'orderby' => 'meta_value',
 'meta_key' => 'podcast_date',
);

$context['posts'] = Timber::get_posts($postArgs);
$context['pagination'] = Timber::get_pagination();

$tag_args = array(
 'taxonomy' => 'podcast_category',
);
$tags = Timber::get_terms($tag_args);
$context['catTags'] = $tags;

$tag_args = array(
 'taxonomy' => 'podcast_name',
);
$tags = Timber::get_terms($tag_args);
$context['nameTags'] = $tags;

$tag_args = array(
 'taxonomy' => 'podcast_host',
);
$tags = Timber::get_terms($tag_args);
$context['hostTags'] = $tags;

$tag_args = array(
 'taxonomy' => 'podcast_guest',
);
$tags = Timber::get_terms($tag_args);
$context['guestTags'] = $tags;

$context['foo'] = get_field('image', get_term(1));

Timber::render('archive-podcast.twig', $context);
