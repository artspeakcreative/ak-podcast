<?php
/*
Plugin Name: ArtSpeak Podcasts
Plugin URI: http://artspeakcreative.com
Description: Podcast archive/player functionality for ArtSpeak sites
Version: 0.1.2
Author: Drew Sartorius
Author URI: http://artspeakcreative.com
License: GPL2
 */

//  PLUGIN FUNCTIONS
function ak_podcast_activation()
{
 // Run when plugin is activated
}
register_activation_hook(__FILE__, 'ak_podcast_activation');

function ak_podcast_deactivation()
{
 // Run when plugin is deactivated
}
register_deactivation_hook(__FILE__, 'ak_podcast_deactivation');

function ak_podcast_uninstall()
{
 // Run when plugin is uninstalled
}
register_uninstall_hook(__FILE__, 'ak_podcast_uninstall');

// HOOKS FUNCTIONS
global $AK_PODCAST_ROOT;
$AK_PODCAST_ROOT = WP_PLUGIN_DIR . '/' . plugin_basename(dirname(__FILE__));
include_once "functions/index.php";

// Add Fields
include_once "fields/podcast_name_fields.php";

// Register Scripts
function ak_podcast_enqueue_scripts()
{
 wp_enqueue_style('ak_podcast-css', plugins_url('/assets/css/podcast.min.css', __FILE__), array(), '', 'all');
//  wp_register_script('script_name', $path, array('jquery'), '', true);
 //  wp_enqueue_script('script_name');
}
add_action('wp_enqueue_scripts', 'ak_podcast_enqueue_scripts');
