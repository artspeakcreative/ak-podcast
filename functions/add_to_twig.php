<?php

function ak_podcast_add_to_twig($twig)
{
 //  PODCAST FUNCTIONS
 $function = new Twig_SimpleFunction('ways_to_listen', function ($podcastName) {
  // Do Something
  $podName = Timber::get_term($podcastName);
  return $podName->ways_to_listen;
 });
 $twig->addFunction($function);

 return $twig;
}
add_filter('akmt_add_to_twig', 'ak_podcast_add_to_twig');
